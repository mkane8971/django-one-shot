from django.shortcuts import render
from todos.models import TodoList

# Create your views here.
def todo_list_list(request):
    todo_lists= TodoList.objects.all()
    context = {
        "todo_object": todo_lists
    }
    return render(request, "todos/lists.html", context)

def todo_list_detail(request, id):
    todo_details= TodoList.objects.get(id=id)
    tasks = todo_details.items.all()
    context ={
        "detail_object": todo_details,
        "tasks": tasks,
    }
    return render(request, "todos/detail.html", context)
